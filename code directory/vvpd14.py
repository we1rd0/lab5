def solution(N):
    """
    Функция принимает на вход положительное число N, затем находит количество разбиений отрезка для получения равнобедренного треугольника.    
    
    Args:
        N:   Положительное число, задающее длину отрезка.
    
    Returns:
        \return None
    
    Raises:
        TypeError
    
    Examples:
        >>> task_solution('a')
        ......................
        TypeError: 'str' object cannot be interpreted as an integer
    """
    loot = [x for x in range(1, N) if (
        x*3 != N) and (N - x > x) and (N - x) % 2 == 0]
    return loot


def input_check(N):
    """
    Данная функция проверяет число на соответствие условиям, необходимым для работы главного модуля программы.
    
    Args:
        N:   Аргумент, не имеет ограничений на тип.
        
    Returns:
        Возвращает True или False(в зависимости от выполнения условия if).
    
    Raises:
        Поднятие исключения невозможно.
    
    Examples:
        Не имеют смысла.
    """
    if type(N) is int and N >= 5:
        return True
    else:
        return False


def main():
    """
    Функция обрабатывает исключение TypeError, вызывает функцию input_check и функцию task_solution.
    
    Args:
        Не принимает аргументов.
    
    Returns:
        None
    
    Raises:
        Единственное возможное исключение(ValueError) обработано.
    
    Examples:
        >>> main()
        'Введите длину отрезка: ' ----> '10'
        'Количество разбиений отрезка для получения равнобедренного треугольника: 2'
    """
    while True:
        try:
            N = int(input("Введите длину отрезка: "))
            if (N < 0):
                print("Длина отрезка не может быть задана отрицательным числом.")
                continue
        except ValueError:
            print("Длина отрезка введена некорректно.")
        else:
            break
    input_check(N)
    print("Количество разбиений отрезка для получения равнобедренного треугольника:", len(solution(N)))
if __name__ == "__main__":
    main()